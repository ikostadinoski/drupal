<?php

namespace Drupal\equipment_scoring\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\equipment_scoring\GetScore;
/**
 * Provides a 'EquipmentScoring' block.
 *
 * @Block(
 *  id = "equipment_scoring",
 *  admin_label = @Translation("Equipment Scoring Block"),
 * )
 */
class EquipmentScoringBlock extends BlockBase implements  ContainerFactoryPluginInterface{

  /**
   * Drupal\Core\Config\CurrentRouteMatch definition.
   *
   * @var CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   *
   * @var EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The entity query.
   *
   * @var \Drupal\Core\Entity\Query\QueryInterface|\Prophecy\Prophecy\ProphecyInterface
   */
  protected $entityQuery;

  /**
   * @var GetScore
   */
  protected $service;


  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param CurrentRouteMatch $currentRouteMatch
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    CurrentRouteMatch $currentRouteMatch,
    EntityTypeManager $entityTypeManager,
    GetScore $service)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentRouteMatch = $currentRouteMatch;
    $this->entityTypeManager = $entityTypeManager;
    $this->service = $service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('entity_type.manager'),
      $container->get('equipment.score')
    );
  }

  public function build() {

    $node_terms = $this->currentRouteMatch->getParameter('node')->get('field_location_equipment')->getValue();
    $stars = $this->service->getScore(count($node_terms));
    $build = [];
    $build['#theme'] = 'equipment_scoring';
    $build['#stars'] = $stars;
    $build['#attached'] = [
      'library' => [
        'equipment_scoring/rating_style',
      ],
    ];
    return $build;
  }

}
