<?php

namespace Drupal\equipment_scoring;

use Drupal\Core\Entity\EntityTypeManagerInterface;


class GetScore {

  private $entityTypeManager;
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager
)
{
  $this->entityTypeManager = $entityTypeManager;
}
  public function getScore($node_terms){
    $all_terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadByProperties([
      'vid' => 'equipment'
    ]);
    $raiting = $node_terms  / count( $all_terms ) * 100 /20 ;
    return intval ($raiting);
  }
}
