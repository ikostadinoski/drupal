<?php

namespace Drupal\Tests\equipment_score\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\equipment_score\GetScore;

/**
 * Simple test to ensure that asserts pass.
 *
 * @group phpunit_example
 */
class ScoreTest extends UnitTestCase {

  protected $unit;

  /**
   * Before a test method is run, setUp() is invoked.
   * Create new unit object.
   */
  public function setUp() {
    $this->unit = new GetScore();
  }

  /**
   * @covers Drupal\event_timer\Unit::setStartDate
   */
  public function testsetStartDate() {

    $equipment = 2;
    $this->assertEquals(1, $equipment);
  }

  /**
   * Once test method has finished running, whether it succeeded or failed, tearDown() will be invoked.
   * Unset the $unit object.
   */
  public function tearDown() {
    unset($this->unit);
  }

}
