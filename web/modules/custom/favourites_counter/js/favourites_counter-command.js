(function ($,Drupal) {
  $(document).ajaxSuccess(function(event, xhr, settings) {

    if (settings.url.includes('/flag/flag') || settings.url.includes('flag/unflag')) {
      jQuery.ajax({
        url: Drupal.url('count'),
        success: function(result) {
          $("#favourites_counter").html(result)
        }
      });
    }
  });
} (jQuery,Drupal));
