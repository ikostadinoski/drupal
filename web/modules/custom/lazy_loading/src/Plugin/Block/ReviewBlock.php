<?php

namespace Drupal\lazy_loading\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Session\AccountProxyInterface;
/**
 *
 * @Block(
 *   id = "lazy_loading",
 *   admin_label = @Translation("Lazy Loading Reviews"),
 *   category = @Translation("Custom"),
 * )
 */
class ReviewBlock extends BlockBase  implements ContainerFactoryPluginInterface,TrustedCallbackInterface {
  /**
   *
   * @var Connection
   */
  protected $connection;

  /**
   *
   * @var EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var AccountProxy
   */
  protected $currentUser;

  /**
   * @param ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('current_user')
    );
  }

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param CurrentRouteMatch $currentRouteMatch
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition,Connection $connection, EntityTypeManager $entityTypeManager, AccountProxyInterface $currentUser)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->connection = $connection;
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
  }

  public function build()
  {
    $build['lazy_builder'] = [
      '#lazy_builder' => [
        ReviewBlock::class . '::lazyBuilder',
        [],
      ],
      '#create_placeholder' => TRUE,
    ];


    return $build;
  }


  public static function lazyBuilder() {

    $currentUserId = \Drupal::currentUser()->id();
    $companyentities = \Drupal::entityTypeManager()->getStorage('review_entity')->loadByProperties(['user_id' => $currentUserId]);
    $review = [];
    foreach ( $companyentities as $key => $entity ) {
      $stars = $entity->get('stars')->getValue()[0]['value'];
      $nid = $entity->get('entity_reference')->getValue()[0]['target_id'];
      $title = \Drupal::entityTypeManager()->getStorage('node')->load($nid)->get('title')->value;
      $review[] = (object) ['title' => $title, 'stars' => $stars];
    }
    sleep(5);
    return  [
      '#theme' => 'reviews_block',
      '#review' => $review,
    ];
  }
  /**
   * {@inheritDoc}
   */
  public static function trustedCallbacks() {
    return ['lazyBuilder'];
  }

}
