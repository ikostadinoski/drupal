<?php

namespace Drupal\Tests\weather_forecast\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\weather_forecast\GetWeatherForecast;

/**
 * Simple test to ensure that asserts pass.
 *
 * @group phpunit_example
 */
class ApiTest extends UnitTestCase {

  protected $unit;

  /**
   * Before a test method is run, setUp() is invoked.
   * Create new unit object.
   */
  public function setUp() {
    $this->unit = new GetWeatherForecast();
  }

  /**
   * @covers Drupal\event_timer\Unit::setStartDate
   */
  public function testApiTest() {

    $response = $this->unit->getForecast('Ljubljana');

    $this->assertEquals(200, $response->cod);
  }

  /**
   * Once test method has finished running, whether it succeeded or failed, tearDown() will be invoked.
   * Unset the $unit object.
   */
  public function tearDown() {
    unset($this->unit);
  }

}
