<?php

namespace Drupal\weather_forecast\Plugin\Block;


use Drupal\Core\Block\BlockBase;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Security\TrustedCallbackInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\weather_forecast\GetWeatherForecast;

/**
 * Provides a 'Weather Forecast' Block.
 *
 * @Block(
 *   id = "weather_forecast",
 *   admin_label = @Translation("Weather Forecast block"),
 *   category = @Translation("Weather Forecast block"),
 * )
 */
class WeatherBlock extends BlockBase implements ContainerFactoryPluginInterface,TrustedCallbackInterface
{

  /**
   *
   * @var CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   *
   * @var GetWeatherForecast
   */
  protected $service;

  /**
   * @var EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * @param ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
      $container->get('weather.forecast')
    );
  }

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param EntityTypeManagerInterface $entityTypeManager
   * @param CurrentRouteMatch $currentRouteMatch
   */
  public function __construct(
    array $configuration,
          $plugin_id,
          $plugin_definition,
    EntityTypeManagerInterface $entityTypeManager,
    CurrentRouteMatch $currentRouteMatch,
    GetWeatherForecast $service
   )
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->currentRouteMatch = $currentRouteMatch;
    $this->service = $service;
  }

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $startDate = $this->currentRouteMatch->getParameter('node')->get('field_start_end_date')->getValue()[0]['value'];
    $days = intval(round(((strtotime($startDate)) - strtotime(date('Y-m-d H:i:s', time()))) / 60 / 60 / 24));
    $locationId = $this->currentRouteMatch->getParameter('node')->get('field_events_location')->getValue()[0]['target_id'];
    $location = $this->entityTypeManager->getStorage('node')->load($locationId);
    $city = 'Ljubljana';
    if ($days < 0) {
      return [
        '#markup' => 'Event has Passed'
      ];
    } elseif ($days > 5) {
      return [
        '#markup' => 'Forecast available as soon as 5 days before the event'
      ];
    } else {
      $build['lazy_builder'] = [
        '#lazy_builder' => [
          WeatherBlock::class . '::lazyBuilder',
          [$days, $city],
        ],
        '#create_placeholder' => TRUE,
      ];

      return $build;
    }
  }

  public static function lazyBuilder($days, $city) {

    $response = \Drupal::service('weather.forecast')->getForecast('Ljubljana');
    sleep(2);
    $build['#theme'] = 'weather_forecast';
    $build['#city'] = $response->city->name;
    $build['#date'] = date('m/d/Y',$response->list[$days]->dt);
    $build['#day'] = $response->list[$days]->temp->day;
    $build['#night'] = $response->list[$days]->temp->night;
    $build['#max'] = $response->list[$days]->temp->max;
    $build['#min'] = $response->list[$days]->temp->min;
    $build['#main'] = $response->list[$days]->weather[0]->main;
    $build['#description'] = $response->list[$days]->weather[0]->description;
    return  $build;
  }

  /**
   * {@inheritDoc}
   */
  public static function trustedCallbacks() {
    return ['lazyBuilder'];
  }
}
