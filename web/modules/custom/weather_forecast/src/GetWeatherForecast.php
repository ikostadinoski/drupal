<?php

namespace Drupal\weather_forecast;

class GetWeatherForecast{

  public static function getForecast($city)
  {
    $apiKey = '542ffd081e67f4512b705f89d2a611b2';
    $response = file_get_contents('http://api.openweathermap.org/data/2.5/forecast/daily?q=' . $city . '&units=metric&cnt=6&appid=' . $apiKey);
    $response = json_decode($response);
    return $response;
  }
}
