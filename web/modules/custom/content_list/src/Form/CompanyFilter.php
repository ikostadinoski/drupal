<?php
namespace Drupal\content_list\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManager;

/**
 * Provides the form for filter Companies.
 */
class CompanyFilter extends FormBase {

  /**
   *
   * @var EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   *
   * @param EntityTypeManager $entityTypeManager
   */
  public function __construct(EntityTypeManager $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_list_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $options = [
      '' => 'Select Industry'
    ];
    $query = $this->entityTypeManager->getStorage('taxonomy_term')->getQuery();
    $nids = $query->condition('vid', "company")->execute();
    $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadMultiple($nids);
    foreach ($terms as $term) {
      $options[$term->tid->value] = $term->name->value;
    }

    $form['filters'] = [
      '#type'  => 'fieldset',
      '#title' => $this->t('Filter'),
      '#open'  => true,
    ];

    $form['filters']['title'] = [
      '#title'         => 'Title',
      '#type'          => 'search',
    ];

    $form['filters']['industry'] = [
      '#title'         => 'Industry',
      '#type'          => 'select',
      '#options'       => $options,
    ];
    $form['filters']['actions'] = [
      '#type'       => 'actions'
    ];

    $form['filters']['actions']['submit'] = [
      '#type'  => 'submit',
      '#value' => $this->t('Filter')

    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array & $form, FormStateInterface $form_state) {
    $field = $form_state->getValues();
    $title = $field["title"];
    $industry = $field["industry"];
    $url = Url::fromRoute('content_list.companies')
      ->setRouteParameters(array('title'=>$title,'industry'=>$industry));
    $form_state->setRedirectUrl($url);
  }

}
