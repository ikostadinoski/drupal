<?php
namespace Drupal\content_list\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class LocationsController.
 *
 * @package Drupal\content_list\Controller
 */
class LocationsController extends ControllerBase
{
  /**
   *
   * @var Connection
   */
  protected $connection;

  /**
   *
   * @var EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   *
   * @var RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a LocationsController object
   * @param Connection $connection
   * @param EntityTypeManager $entityTypeManager
   */
  public function __construct(Connection $connection, EntityTypeManager $entityTypeManager, RequestStack $requestStack) {
    $this->connection = $connection;
    $this->entityTypeManager = $entityTypeManager;
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('request_stack')
    );
  }

  public function getLocations() {
    $title = $this->requestStack->getCurrentRequest()->query->get('title');
    $equipment = $this->requestStack->getCurrentRequest()->query->get('equipment');

    $output = $this->filter_locations("", $title, $equipment);

    if ($title == "" && $equipment == "") {
      $output = $this->filter_locations("All", "", "");
    }

    $form['form'] = $this->formBuilder()->getForm('Drupal\content_list\Form\LocationFilter');
    $form['list'] = $output;
    return $form;
  }

  public function filter_locations($opt, $title, $equipment)
  {
    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    if ($equipment !== '') {
      $nids = $query
        ->condition('type','location')
        ->condition('field_location_equipment',$equipment,'=')
        ->condition('title',$title,'CONTAINS')
        ->execute();
    }
    else {
      $nids = $query
        ->condition('type','location')
        ->condition('title',$title,'CONTAINS')
        ->execute();
    }
    $rendered = [];
    foreach ( $nids as $key => $nid ) {
      $entity_type = 'node';
      $view_mode = 'teaser';

      $node = $this->entityTypeManager->getStorage($entity_type)->load($nid);
      $element = $this->entityTypeManager->getViewBuilder($entity_type)->view($node, $view_mode);

      array_push($rendered, $element);
    }
    return $rendered;
  }
}
