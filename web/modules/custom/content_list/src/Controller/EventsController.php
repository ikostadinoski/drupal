<?php
namespace Drupal\content_list\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class CompaniesController.
 *
 * @package Drupal\content_list\Controller
 */
class EventsController extends ControllerBase
{
  /**
   *
   * @var Connection
   */
  protected $connection;

  /**
   *
   * @var EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   *
   * @var RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a CompaniesController object
   * @param Connection $connection
   * @param EntityTypeManager $entityTypeManager
   */
  public function __construct(Connection $connection, EntityTypeManager $entityTypeManager, RequestStack $requestStack) {
    $this->connection = $connection;
    $this->entityTypeManager = $entityTypeManager;
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('request_stack')
    );
  }

  public function getEvents() {
    $title = $this->requestStack->getCurrentRequest()->query->get('title');
    $event_type = $this->requestStack->getCurrentRequest()->query->get('field_events_event_type');
    $output = $this->filter_events("", $title, $event_type);

    if ($title == "" && $event_type == "") {
      $output = $this->filter_events("All", "", "");
    }

    $form['form'] = $this->formBuilder()->getForm('Drupal\content_list\Form\EventFilter');
    $form['list'] = $output;
    return $form;
  }

  public function filter_events($opt, $title, $event_type)
  {
    $query = $this->entityTypeManager->getStorage('node')->getQuery();

    if ($event_type !== '') {
      $nids = $query
        ->condition('type','events')
        ->condition('field_events_event_type',$event_type,'=')
        ->condition('title',$title,'CONTAINS')
        ->execute();
    }
    else {
      $nids = $query
        ->condition('type','events')
        ->condition('title',$title,'CONTAINS')
        ->execute();
    }
    $rendered = [];
    foreach ( $nids as $key => $nid ) {
      $entity_type = 'node';
      $view_mode = 'teaser';

      $node = $this->entityTypeManager->getStorage($entity_type)->load($nid);
      $element = $this->entityTypeManager->getViewBuilder($entity_type)->view($node, $view_mode);
      array_push($rendered, $element);
    }
    return $rendered;
  }
}
