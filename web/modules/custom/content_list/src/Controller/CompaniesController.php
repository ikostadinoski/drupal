<?php
namespace Drupal\content_list\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class CompaniesController.
 *
 * @package Drupal\content_list\Controller
 */
class CompaniesController extends ControllerBase
{
  /**
   *
   * @var Connection
   */
  protected $connection;

  /**
   *
   * @var EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   *
   * @var RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a CompaniesController object
   * @param Connection $connection
   * @param EntityTypeManager $entityTypeManager
   */
  public function __construct(Connection $connection, EntityTypeManager $entityTypeManager, RequestStack $requestStack) {
    $this->connection = $connection;
    $this->entityTypeManager = $entityTypeManager;
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('request_stack')
    );
  }

  public function getCompanies() {
    $title = $this->requestStack->getCurrentRequest()->query->get('title');
    $industry = $this->requestStack->getCurrentRequest()->query->get('industry');
    $output = $this->filter_companies("", $title, $industry);

    if ($title == "" && $industry == "") {
      $output = $this->filter_companies("All", "", "");
    }

    $form['form'] = $this->formBuilder()->getForm('Drupal\content_list\Form\CompanyFilter');
    $form['list'] = $output;
    return $form;
  }

  public function filter_companies($opt, $title, $industry)
  {
    $query = $this->entityTypeManager->getStorage('node')->getQuery();

   if ($industry !== '') {
      $nids = $query
        ->condition('type','company')
        ->condition('field_comapnyld_industry',$industry,'=')
        ->condition('title',$title,'CONTAINS')
        ->execute();
    }
    else {
      $nids = $query
        ->condition('type','company')
        ->condition('title',$title,'CONTAINS')
        ->execute();
    }
    $rendered = [];
    foreach ( $nids as $key => $nid ) {
      $entity_type = 'node';
      $view_mode = 'teaser';

      $node = $this->entityTypeManager->getStorage($entity_type)->load($nid);
      $element = $this->entityTypeManager->getViewBuilder($entity_type)->view($node, $view_mode);
      array_push($rendered, $element);
    }
    return $rendered;
  }
}
