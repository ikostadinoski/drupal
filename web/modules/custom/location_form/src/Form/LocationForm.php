<?php

/**
 * @file
 * Contains \Drupal\status_message\Form\StatusMessageForm.
 */

namespace Drupal\location_form\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\node\Entity\Node;
use Drupal\location_form\GetSuggestionAdminsEmails;

class LocationForm extends ConfigFormBase {

  /**
   *
   * @var MailManagerInterface
   */
  protected $mailManager;

  /**
   *
   * @var EmailValidator
   */
  protected $emailValidator;

  /**
   *
   * @var EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   *
   * @var AccountProxy
   */
  protected $accountProxy;

  /**
   * The language manager.
   *
   * @var LanguageManagerInterface
   */
  protected $languageManager;

  /**
   *
   * @var MailManagerInterface
   */
  protected $service;


  public function __construct(
    EntityTypeManager $entityTypeManager,
    AccountProxy $accountProxy,
    LanguageManagerInterface $language_manager,
    MailManagerInterface $mail_manager,
    GetSuggestionAdminsEmails $service) {
    $this->entityTypeManager = $entityTypeManager;
    $this->accountProxy = $accountProxy;
    $this->languageManager = $language_manager;
    $this->mailManager = $mail_manager;
    $this->service = $service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('language_manager'),
      $container->get('plugin.manager.mail'),
      $container->get('location_form.emails'),
    );
  }
  /**
   * {@inheritdoc}
   */
  public function getFormId() {

    return 'location_form';

  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadByProperties([
      'vid' => 'equipment'
    ]);
    $userId = $this->accountProxy->id();
    $user = $this->entityTypeManager->getStorage('user')->load($userId);
    $email = $user->get('mail')->getValue()[0]['value'];
    foreach ($terms as $term) {
      $options[$term->tid->value] = $term->name->value;
    }
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Name:'),
      '#required' => TRUE,
    );
    $form['description'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Description:'),
      '#required' => TRUE,
    );
    $form['email'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Contact email:'),
      '#required' => TRUE,
    );
    $form['website'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Website:'),
      '#required' => TRUE,
    );
    $form['equipment'] = [
      '#title'         => 'Equipment',
      '#type'          => 'checkboxes',
      '#options'       => $options,
    ];
    $form['your_name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Your Name:'),
      '#required' => TRUE,
    );
    $form['your_email'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Your Email:'),
      '#default_value' => $email,
      '#required' => TRUE,
    );
    $form['connection'] = array(
      '#type' => 'radios',
      '#required' => TRUE,
      '#title' => $this
        ->t('Your connection to the Location ?'),
      '#options' => array(
        'I own this Location' => $this->t('I own this Location'),
        'Im working at this location' => $this->t('Im working at this location'),
        'I only know this place because I visited it before.' => $this->t('I only know this place because I visited it before.'),
        'Other.' => $this->t('Other.'),
      '#attributes' => [
          'name' => 'connection',
        ],
      '#states' => [
          'enabled' => [
            //enable the radios only if the custom text is empty
            ':input[name="field_custom_text"]' => ['value' => ''],
          ],
        ],
      ),
    );
    $form['custom'] = [
      '#type' => 'textfield',
      '#size' => '60',
      '#placeholder' => 'Your connection to the Location',
      '#attributes' => [
        //add static name and id to the textbox
        'id' => 'custom',
        'name' => 'field_custom_text',
      ],
      '#states' => [
        //show this textfield only if the radio 'other' is selected above
        'visible' => [
          ':input[name="connection"]' => ['value' => 'Other.'],
        ],
      ],
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit Location'),
      '#button_type' => 'primary',
    ];
    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if (filter_var($form_state->getValue('name') === '')) {
      return array(
        'message' => $this->t('Please enter valid name.'),
        'valid' => false
      );
    } elseif (!filter_var($form_state->getValue('email'), FILTER_VALIDATE_EMAIL)) {
      return array(
        'message' => $this->t('Please enter valid email.'),
        'valid' => false
      );
    }
    elseif (filter_var($form_state->getValue('description') === '')) {
      return array(
        'message' => $this->t('Please enter valid description.'),
        'valid' => false
      );
    }
    elseif (filter_var($form_state->getValue('your_name') === '')) {
      return array(
        'message' => $this->t('Please enter your Name.'),
        'valid' => false
      );
    } elseif (!filter_var($form_state->getValue('your_email'), FILTER_VALIDATE_EMAIL)) {
      return array(
        'message' => $this->t('Please enter valid email.'),
        'valid' => false
      );

    }
    else {
      return array(
        'message' => $this->t('Valid fields.'),
        'valid' => true
      );
    }
  }


  /**
   * {@inheritdoc}
   */


  public function submitForm(array &$form, FormStateInterface $form_state) {

    $equipment = [];
    global $base_url;

    $language = $this->languageManager->getCurrentLanguage()->getId();
    foreach ($form_state->getValue('equipment') as $item){
      if ($item !== 0) {
        array_push($equipment, $item);
      }
    }

    $node = Node::create(array(
      'type' => 'location',
      'title' => $form_state->getValue('name'),
      'field_location_description' => $form_state->getValue('description'),
      'field_location_contact_email	' => $form_state->getValue('email'),
      'field_location_address' => $form_state->getValue('address'),
      'field_location_website	' => $form_state->getValue('website'),
      'field_location_equipment' => $equipment,
      'field_is_operating' => true,
      'langcode' => $language,
      'status' => 0,
    ));

    $isValid = $this->validateForm($form, $form_state);
    if ( !$isValid['valid'] ) {
      $this->messenger()->addError($isValid['message']);
    } else {

      $node->save();

      $module = 'location_form';
      $key = 'general';
      $params['node_title'] = $form_state->getValue('title');
      $params['node_url'] = ''. $base_url .''. $node->toUrl()->toString() .'';
      $params['name'] = $form_state->getValue('name');
      $params['description'] = $form_state->getValue('description');
      $params['equipment'] = $form_state->getValue('equipment');
      $send = true;

      foreach ($this->service->getEmails() as $email){
        $result = $this->mailManager->mail($module, $key, $email, $language, $params, NULL, $send);
      }
      $this->messenger()->addStatus($this->t('Your Location is submited successfuly.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'location_form.settings',
    ];
  }
}
