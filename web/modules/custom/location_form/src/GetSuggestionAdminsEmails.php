<?php

namespace Drupal\location_form;

use Drupal\Core\Entity\EntityTypeManagerInterface;

class GetSuggestionAdminsEmails{

  private $entityTypeManager;

  public function __construct(
    EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  public function getEmails(){
    $emails = [];
    $userStorage = $this->entityTypeManager->getStorage('user');
    $query = $userStorage->getQuery();
    $uids = $query
      ->condition('status', '1')
      ->condition('roles', 'suggestions_admin')
      ->execute();
    $users = $userStorage->loadMultiple($uids);
    foreach ($users as $user){
      $email = $user->get('mail')->getValue()[0]['value'];
      array_push($emails, $email);
    }
    return $emails;
  }
}
