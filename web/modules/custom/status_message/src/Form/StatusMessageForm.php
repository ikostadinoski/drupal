<?php

/**
 * @file
 * Contains \Drupal\status_message\Form\StatusMessageForm.
 */

namespace Drupal\status_message\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class StatusMessageForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {

    return 'status_message_form';

  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('status_message.settings');
    $form['enabled'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Message'),
      '#default_value' => $config->get('status_message.enabled'),
    );
    $form['message'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Message:'),
      '#default_value' => $config->get('status_message.message'),
    );
    $form['message_type'] = array(
      '#type' => 'select',
      '#options' => [
        'alert' => t('Alert Message'),
        'warning' => t('Warning Message'),
        'info' => t('Info Message')
      ],
      '#title' => $this->t('Type:'),
      '#default_value' => $config->get('status_message.message_type'),
    );
    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('status_message.settings');
    $config->set('status_message.enabled', $form_state->getValue('enabled'));
    $config->set('status_message.message', $form_state->getValue('message'));
    $config->set('status_message.message_type', $form_state->getValue('message_type'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'status_message.settings',
    ];
  }
}
