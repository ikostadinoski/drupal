<?php

namespace Drupal\status_message\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
/**
 * Provides a 'StatusMessageBlock' block.
 *
 * @Block(
 *  id = "status_message_block",
 *  admin_label = @Translation("Status message block"),
 * )
 */
class StatusMessageBlock extends BlockBase implements  ContainerFactoryPluginInterface{

  /**
   * @var $configFactory ConfigFactoryInterface
   */
  protected $configFactory;


  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param ConfigFactoryInterface $configFactory
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $configFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $configFactory;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }
  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $enabled = $this->configFactory->get('status_message.settings')->get('status_message')['enabled'];
    if ($enabled) {
      $message = $this->configFactory->get('status_message.settings')->get('status_message')['message'];
      $messageType = $this->configFactory->get('status_message.settings')->get('status_message')['message_type'];
      $build['#theme'] = 'message';
      $build['#mes'] = $message;
      $build['#type'] = $messageType;
      $build['#attached'] = [
        'library' => [
          'status_message/style',
        ],
      ];
    }
    return $build;
  }

}
