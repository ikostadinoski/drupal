<?php

namespace Drupal\Tests\status_message\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests for the status_message module.
 *
 * @group status_message
 */
class StatusTest extends BrowserTestBase
{

  /**
   * A simple user.
   * @var \Drupal\user\Entity\User
   */
  private $user;

  /**
   * Perform initial setup tasks that run before every test method.
   */
  /**
   * Tests the config form.
   */

  public function setUp()
  {
    parent::setUp();
    $this->user = $this->drupalCreateUser(array(
      'administer site configuration',
      'test status message',
    ));
  }


  public function testConfigForm()
  {
    // Login.
    $this->drupalLogin($this->user);

    // Access config page.
    $this->drupalGet('admin/config/status/config');
    $this->assertSession()->statusCodeEquals(200);
    // Test the form elements exist and have defaults.
    $config = $this->config('status_message.settings');
    $this->assertSession()->fieldValueEquals(
      'message',
      $config->get('status_message.message'),
    );
  }
}
