<?php

namespace Drupal\configuration_example\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\configuration_example\Entity\ConfigurationExampleInterface;

/**
 * Defines the Example entity.
 *
 * @ConfigEntityType(
 *   id = "configuration_example",
 *   label = @Translation("Configuration Example"),
 *   handlers = {
 *     "list_builder" = "Drupal\configuration_example\Controller\ConfigurationExampleListBuilder",
 *     "form" = {
 *       "add" = "Drupal\configuration_example\Form\ConfigurationExampleForm",
 *       "edit" = "Drupal\configuration_example\Form\ConfigurationExampleForm",
 *       "delete" = "Drupal\configuration_example\Form\ConfigurationExampleDeleteForm",
 *     }
 *   },
 *   config_prefix = "configuration_example",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "url" = "url",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "url"
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/system/configuration_example/{configuration_example}",
 *     "delete-form" = "/admin/config/system/configuration_example/{configuration_example}/delete",
 *   }
 * )
 */
class ConfigurationExample extends ConfigEntityBase implements ConfigurationExampleInterface{

  /**
   * The Example ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Example label.
   *
   * @var string
   */
  protected $label;

  /**
   * Url.
   *
   * @var string
   */
  protected $url;


}
