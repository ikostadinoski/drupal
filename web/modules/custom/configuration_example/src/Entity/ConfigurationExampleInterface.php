<?php

namespace Drupal\configuration_example\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an Example entity.
 */
interface ConfigurationExampleInterface extends ConfigEntityInterface {
}
