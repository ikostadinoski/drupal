<?php
namespace Drupal\field_yt\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * @FieldWidget(
 *     id = "default_widget",
 *     label = @Translation("Youtube default Widget"),
 *     field_types = {
 *          "field_yt"
 *     }
 * )
 */
class YTWidget extends WidgetBase
{

  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state)
  {
    $elements = [];

    $elements['value'] = [
      '#type' => 'url',
      '#title' => t('Youtube Link:')
    ];

    return $elements;
  }
}
