<?php

namespace Drupal\field_yt\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides a field type of field_yt.
 *
 * @FieldType(
 *   id = "field_yt",
 *   label = @Translation("Youtube field"),
 *   default_formatter = "default_formatter",
 *   default_widget = "default_widget",
 * )
 */
class YTFieldItem extends FieldItemBase {

  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition)
  {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Youtube Link'));
    return $properties;
  }

  public static function schema(FieldStorageDefinitionInterface $field_definition)
  {
    return [
      'columns' => [
        'value' => [
          'type' => 'text',
          'description' => t('Youtube Link'),
        ],
      ],
      'indexes' => [
        'value' => ['value'],
      ]
    ];
  }
}
