<?php
namespace Drupal\field_yt\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * @FieldFormatter(
 *     id = "embed_formatter",
 *     label = @Translation("YT Embed"),
 *     field_types = {
 *          "field_yt"
 *     }
 * )
 */
class YTEmbedFormatter extends FormatterBase {

  public function viewElements(FieldItemListInterface $items, $langcode)
  {
    $linkValue = NULL;
    if (!$items->isEmpty()) {
      $linkValue = $items->getValue()[0]['value'];
      $linkValue = str_replace('watch?v=','embed/',$linkValue);
    }
    $elements = [];
    $elements[0] = [
      '#theme' => 'video_embed',
      '#url' => $linkValue
    ];

    return $elements;
  }

}
