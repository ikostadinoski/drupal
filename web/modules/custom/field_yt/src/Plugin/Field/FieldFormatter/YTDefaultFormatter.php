<?php
namespace Drupal\field_yt\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * @FieldFormatter(
 *     id = "default_formatter",
 *     label = @Translation("YT Link"),
 *     field_types = {
 *          "field_yt"
 *     }
 * )
 */
class YTDefaultFormatter extends FormatterBase {

  public function viewElements(FieldItemListInterface $items, $langcode)
  {
    $mode = $this->getSetting('mode');
    $linkValue = NULL;
    if (!$items->isEmpty()) {
      $linkValue = $items->getValue()[0]['value'];
    }
    $elements = [];
    $elements[0] = [
      '#theme' => 'video_link',
      '#mode' => $mode,
      '#url' => $linkValue
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['mode'] = [
      '#title' => $this->t('Open in:'),
      '#type' => 'select',
      '#options' => [
        '_blank' => $this->t('Blank Page'),
        '_self' => $this->t('Current Page'),
      ],
      '#default_value' => $this->getSetting('text_length'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'mode' => '_blank',
      ] + parent::defaultSettings();
  }
}

