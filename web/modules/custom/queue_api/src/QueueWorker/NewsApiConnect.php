<?php
namespace Drupal\queue_api\QueueWorker;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use jcobhams\NewsApi\NewsApi;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Database\Connection;

/**
 *
 * @QueueWorker(
 *      id = "queue_api2",
 *      title = "News Queue",
 *      cron = {"time" = 10}
 * )
 *
 * @package Drupal\queue_api\Plugin\QueueWorker
 */
class NewsApiConnect extends QueueWorkerBase implements ContainerFactoryPluginInterface{

  /**
  Connection   *
   * @var EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   *
   * @var Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
          $plugin_id,
          $plugin_definition,
    EntityTypeManager $entityTypeManager,
    Connection $connection
  )
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->connection = $connection;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('database')
    );
  }

  public function processItem($item)
  {
    $event = $this->entityTypeManager->getStorage('node')->load($item->nid);
    $field_id = $event->get('field_events_organizer')->getValue()[0]['target_id'];
    $organizer = $this->entityTypeManager->getStorage('node')->load($field_id);
    $searchTerm = $organizer->getTitle();
    $newsapi = new NewsApi('edf8398a86e7497a9a79ce19be112794');
    $news = $newsapi->getEverything($searchTerm, NULL, NULL, NULL, NULL, NULL, 'en', NULL,  3, NULL);
    foreach ($news->headlines as $headline){
      $articleTitle = $headline->title;
      $articleUrl = $headline->url;
      $this->connection->insert('queue_api')
        ->fields([
          'title' => $articleTitle,
          'url' => $articleUrl,
          'uids' => $item->nid,
        ])
        ->execute();
    }
  }


}
