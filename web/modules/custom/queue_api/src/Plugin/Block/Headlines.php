<?php

namespace Drupal\queue_api\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Database\Connection;

/**
 * Provides a 'Articles' block.
 *
 * @Block(
 *  id = "headlines",
 *  admin_label = @Translation("Headlines"),
 * )
 */
class Headlines extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   *
   * @var CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   *
   * @var Connection
   */
  protected $connection;


  /**
   * @param ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('current_route_match')
    );
  }

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param Connection $connection
   * @param CurrentRouteMatch $currentRouteMatch
   */
  public function __construct(
    array $configuration,
          $plugin_id,
          $plugin_definition,
    Connection $connection,
    CurrentRouteMatch $currentRouteMatch)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->connection = $connection;
    $this->currentRouteMatch = $currentRouteMatch;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $headlinesList = [];
    $eventId = $this->currentRouteMatch->getParameter('node')->id();
    $headlines = $this->connection->query("SELECT title,url FROM queue_api WHERE uids = :value", [':value' => $eventId])->fetchAll();
    foreach ($headlines as $key => $headline) {
      $articleList[$key]['title'] = $headline->title;
      $articleList[$key]['url'] = $headline->url;
    }
    $build = [];
    $build['#theme'] = 'articles';
    $build['#content'] = $headlinesList;
    return $build;
  }

}
