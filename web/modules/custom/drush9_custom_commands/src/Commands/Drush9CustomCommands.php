<?php

namespace Drupal\drush9_custom_commands\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\search_api\Utility\CommandHelper;
use Drush\Commands\DrushCommands;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


/**
 * A drush command file.
 *
 * @package Drupal\drush9_custom_commands\Commands
 */
class Drush9CustomCommands extends DrushCommands
{

  /**
   * The command helper.
   *
   * @var \Drupal\search_api\Utility\CommandHelper
   */
  protected $commandHelper;

  /**
   * Constructs a SearchApiCommands object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher.
   *
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, ModuleHandlerInterface $moduleHandler, EventDispatcherInterface $eventDispatcher) {
    parent::__construct();

    $this->commandHelper = new CommandHelper($entityTypeManager, $moduleHandler, $eventDispatcher);
  }

  /**
   * {@inheritdoc}
   */
  public function setLogger(LoggerInterface $logger) {
    parent::setLogger($logger);
    $this->commandHelper->setLogger($logger);
  }

  /**
   * Drush command for reindexing.
   *
   * * @param string $indexId
   *   The machine name of an index. Optional. If missed, will schedule all
   *   search indexes for reindexing.
   *
   * @param array $options
   *   An array of options.
   *
   * @command drush9_custom_commands:reindex
   *
   * @aliases d9-reindex
   *
   * @option reverse
   */

  public function reindex($indexId = NULL, array $options = ['limit' => NULL, 'batch-size' => NULL]) {
    $limit = $options['limit'];
    $batch_size = $options['batch-size'];
    $process_batch = $this->commandHelper->indexItemsToIndexCommand(['company'], $limit, $batch_size);

    if ($process_batch === TRUE) {
      drush_backend_batch_process();
    }
  }
}


