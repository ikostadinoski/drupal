<?php

namespace Drupal\search_api_field;

use Drupal\Core\Entity\EntityTypeManagerInterface;

  class GetCompaniesService{

    private $entityTypeManager;

    public function __construct(
      EntityTypeManagerInterface $entityTypeManager) {
      $this->entityTypeManager = $entityTypeManager;
    }

    public function getNids(){

      $events = $this->entityTypeManager->getStorage('node')
        ->loadByProperties(['type' => 'events', 'status' => 1]);
      $nids = [];
      foreach ($events as $event) {
        $eventStart = $event->get('field_start_end_date')->getValue()[0]['value'];
        $current = date('Y-m-d H:i:s', time());
        $diffStart = ((strtotime($current) - strtotime($eventStart)) / 60 / 60 / 24);
        if ($diffStart < 0) {
          $nid = $event->get('field_events_organizer')->getValue()[0]['target_id'];
          array_push($nids, $nid);
        }
      }
      return $nids;
  }
}
