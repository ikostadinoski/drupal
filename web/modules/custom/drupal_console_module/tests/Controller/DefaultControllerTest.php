<?php

namespace Drupal\drupal_console_module\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the drupal_console_module module.
 */
class DefaultControllerTest extends WebTestBase {


  /**
     * {@inheritdoc}
     */
  public static function getInfo() {
        return [
            'name' => "drupal_console_module DefaultController's controller functionality",
            'description' => 'Test Unit for module drupal_console_module and controller DefaultController.',
            'group' => 'Other',
          ];
  }

  /**
     * {@inheritdoc}
    */
  public function setUp() {
        parent::setUp();
      }
        /**
     * Tests drupal_console_module functionality.
     */
  public function testDefaultController() {
        // Check that the basic functions of module drupal_console_module.
        $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
      }

}
