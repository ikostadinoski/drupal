<?php

namespace Drupal\drupal_console_module\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class DefaultController.
 */
class DefaultController extends ControllerBase {

  /**
     * Welcome.
     *
    * @return string
     *   Return Hello string.
     */
  public function welcome() {
        return [
            '#type' => 'markup',
            '#markup' => $this->t('Implement method: welcome')
            ];
  }
}
