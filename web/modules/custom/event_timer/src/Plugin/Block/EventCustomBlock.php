<?php

namespace Drupal\event_timer\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\event_timer\CountEventTime;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;




/**
 * Provides a 'Events Timer' Block.
 *
 * @Block(
 *   id = "event_timer",
 *   admin_label = @Translation("Event Custom Timer"),
 *   category = @Translation("Events Block"),
 * )
 */
class EventCustomBlock extends BlockBase implements ContainerFactoryPluginInterface
{

  /**
   * @var $service CountEventTime
   */
  protected $service;

  /**
   * @var $currentRouteService CurrentRouteMatch
   */
  protected $currentRouteService;

  /**
   * @param ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('event_timer.count_time'),
      $container->get('current_route_match')
    );
  }

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param CountEventTime $service
   * @param CurrentRouteMatch $currentRouteMatch
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CountEventTime $service, CurrentRouteMatch $currentRouteMatch)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->service=$service;
    $this->currentRouteService = $currentRouteMatch;
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array
  {

    $node = $this->currentRouteService->getParameter('node');
    $time_info = "";
    if ($node && $node->getType() === 'events') {
      $eventStart = $node->get('field_start_end_date')->getValue()[0]['value'];
      $eventEnd = $node->get('field_start_end_date')->getValue()[0]['end_value'];
      $this->service->setStartDate($eventStart);
      $this->service->setEndDate($eventEnd);
      $time_info = $this->service->count_time();
    }
    return [
      '#theme' => 'event_block',
      '#time_info' => $time_info,
    ];
  }
  public function getCacheMaxAge(): int
  {
    return 0;
  }
}
