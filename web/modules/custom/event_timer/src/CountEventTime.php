<?php

namespace Drupal\event_timer;

/**
 * Class CountEventTime.
 * @package Drupal\event_timer
 */
class CountEventTime
{

  public function setStartDate($eventStart) {
    $this->eventStart = $eventStart;
  }

  public function getStartDate() {
    return $this->eventStart;
  }

  public function setEndDate($eventEnd) {
    $this->eventEnd = $eventEnd;
  }

  public function getEndDate() {
    return $this->eventEnd;
  }

  public function count_time()
  {
    $current = date('Y-m-d H:i:s', time());
    $diffStart = ((strtotime($current) - strtotime($this->getStartDate())) / 60 / 60 / 24);
    $diffEnd = ((strtotime($current) - strtotime($this->getEndDate())) / 60 / 60 / 24);
    if ($diffStart < 0) {
      return t("Time left to event start: ") . abs(round($diffStart)) . t(' days.');
    } elseif (($diffStart >= 0) and (($diffStart <= 1))) {
      return t('This event is happening today');
    } elseif (($diffStart >= 0) and ($diffEnd < 1)) {
      return t('The event is currently in progress.');
    } elseif (($diffStart >= 0) and ($diffEnd > 1)) {
      return t('The event has passed.');
    }
  }
}
