<?php

namespace Drupal\Tests\event_timer\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\event_timer\CountEventTime;

/**
 * Simple test to ensure that asserts pass.
 *
 * @group phpunit_example
 */
class UnitTest extends UnitTestCase {

  protected $unit;

  /**
   * Before a test method is run, setUp() is invoked.
   * Create new unit object.
   */
  public function setUp() {
    $this->unit = new CountEventTime();
  }

  /**
   * @covers Drupal\event_timer\Unit::setStartDate
   */
  public function testsetStartDate() {

    $this->unit->setStartDate('2021-08-04 13:19:08');
    $this->unit->setEndDate('2021-08-05 13:30:08');

    $days = intval((strtotime($this->unit->getEndDate()) - strtotime($this->unit->getStartDate())) / 60 /60 /24);

    $this->assertEquals(1, $days);
  }

  /**
   * Once test method has finished running, whether it succeeded or failed, tearDown() will be invoked.
   * Unset the $unit object.
   */
  public function tearDown() {
    unset($this->unit);
  }

}
