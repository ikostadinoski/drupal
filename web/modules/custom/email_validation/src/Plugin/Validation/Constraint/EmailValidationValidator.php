<?php
namespace Drupal\email_validation\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
* Validates the Email field.
*/
  class EmailValidationValidator extends ConstraintValidator {

  /**
  * {@inheritdoc}
  */
    public function validate($items, Constraint $constraint) {
      $item = $items->first()->getValue()['value'];
        if (str_starts_with($item, 'no-reply')) {
          $this->context->addViolation($constraint->emailValidation);
     }
  }
}
