<?php
namespace Drupal\email_validation\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if email field start with 'no-reply'
 *
 * @Constraint(
 *   id = "emailValidation",
 *   label = @Translation("emailValidation", context = "Validation"),
 * )
 */
class EmailValidation extends Constraint {

  // The message that will be shown if the format is incorrect.
  public string $emailValidation = $this->t('The email can not start with "no-reply"');
}
