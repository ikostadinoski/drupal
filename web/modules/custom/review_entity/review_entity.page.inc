<?php

/**
 * @file
 * Contains review_entity.page.inc.
 *
 * Page callback for Review entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Review entity templates.
 *
 * Default template: review_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_review_entity(array &$variables) {
  // Fetch ReviewEntity Entity Object.
  $review_entity = $variables['elements']['#review_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
