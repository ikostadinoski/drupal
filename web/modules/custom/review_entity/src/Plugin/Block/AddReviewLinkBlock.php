<?php

namespace Drupal\review_entity\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;


/**
 *
 * @Block(
 *   id = "rever",
 *   admin_label = @Translation("Add Review Link"),
 *   category = @Translation("Add Review Link"),
 * )
 */
class AddReviewLinkBlock extends BlockBase implements ContainerFactoryPluginInterface
{

  /**
   * @var $currentRouteService CurrentRouteMatch
   */
  protected $currentRouteService;

  /**
   * @param ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match')
    );
  }

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param CurrentRouteMatch $currentRouteMatch
   */
  public function __construct(
    array                 $configuration,
                          $plugin_id,
                          $plugin_definition,
    CurrentRouteMatch     $currentRouteMatch)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentRouteService = $currentRouteMatch;
  }

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $companyReview = 'company_review';
    $locationReview = 'location_review';
    $path = '';
    $node = $this->currentRouteService->getParameter('node');
    $type = $node->getType();
    if ($type === 'location') {
      $path = $locationReview;
    }
    elseif ($type === 'company') {
      $path = $companyReview;
    }
    $url = Url::fromRoute('entity.review_entity.add_page',['review_entity_type'=>$path]);
    $args['node'] = $node->id();
    $url->setOptions(array('query' => $args));
    $link = Link::fromTextAndUrl($this->t('Add Review'), $url);
    $build['review'] = $link->toRenderable();
    return $build;
  }
}
