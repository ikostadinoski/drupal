<?php

namespace Drupal\review_entity\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Review entity entities.
 *
 * @ingroup review_entity
 */
class ReviewEntityDeleteForm extends ContentEntityDeleteForm {


}
