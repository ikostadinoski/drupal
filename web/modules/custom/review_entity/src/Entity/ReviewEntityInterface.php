<?php

namespace Drupal\review_entity\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Review entity entities.
 *
 * @ingroup review_entity
 */
interface ReviewEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Review entity name.
   *
   * @return string
   *   Name of the Review entity.
   */
  public function getName();

  /**
   * Sets the Review entity name.
   *
   * @param string $name
   *   The Review entity name.
   *
   * @return \Drupal\review_entity\Entity\ReviewEntityInterface
   *   The called Review entity entity.
   */
  public function setName($name);

  /**
   * Gets the Review entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Review entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Review entity creation timestamp.
   *
   * @param int $timestamp
   *   The Review entity creation timestamp.
   *
   * @return \Drupal\review_entity\Entity\ReviewEntityInterface
   *   The called Review entity entity.
   */
  public function setCreatedTime($timestamp);

}
