<?php

namespace Drupal\review_entity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Review entity type entities.
 */
interface ReviewEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
