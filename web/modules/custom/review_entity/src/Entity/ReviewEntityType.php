<?php

namespace Drupal\review_entity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Review entity type entity.
 *
 * @ConfigEntityType(
 *   id = "review_entity_type",
 *   label = @Translation("Review entity type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\review_entity\ReviewEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\review_entity\Form\ReviewEntityTypeForm",
 *       "edit" = "Drupal\review_entity\Form\ReviewEntityTypeForm",
 *       "delete" = "Drupal\review_entity\Form\ReviewEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\review_entity\ReviewEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "review_entity_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "review_entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "enabled",
 *     "first_field",
 *     "second_field",
 *     "bundles"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/review_entity_type/{review_entity_type}",
 *     "add-form" = "/admin/structure/review_entity_type/add",
 *     "edit-form" = "/admin/structure/review_entity_type/{review_entity_type}/edit",
 *     "delete-form" = "/admin/structure/review_entity_type/{review_entity_type}/delete",
 *     "collection" = "/admin/structure/review_entity_type"
 *   }
 * )
 */
class ReviewEntityType extends ConfigEntityBundleBase implements ReviewEntityTypeInterface {

  /**
   * The Review entity type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Review entity type label.
   *
   * @var string
   */
  protected $label;

}
