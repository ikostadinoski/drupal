<?php

namespace Drupal\review_entity\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Review entity entity.
 *
 * @ingroup review_entity
 *
 * @ContentEntityType(
 *   id = "review_entity",
 *   label = @Translation("Review entity"),
 *   bundle_label = @Translation("Review entity type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\review_entity\ReviewEntityListBuilder",
 *     "views_data" = "Drupal\review_entity\Entity\ReviewEntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\review_entity\Form\ReviewEntityForm",
 *       "add" = "Drupal\review_entity\Form\ReviewEntityForm",
 *       "edit" = "Drupal\review_entity\Form\ReviewEntityForm",
 *       "delete" = "Drupal\review_entity\Form\ReviewEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\review_entity\ReviewEntityHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\review_entity\ReviewEntityAccessControlHandler",
 *   },
 *   base_table = "review_entity",
 *   translatable = FALSE,
 *   permission_granularity = "bundle",
 *   admin_permission = "administer review entity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "enabled",
 *     "first_field",
 *     "second_field",
 *     "bundles"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/review_entity/{review_entity}",
 *     "add-page" = "/admin/structure/review_entity/add/{review_entity_type}",
 *     "add-form" = "/admin/structure/review_entity/add/{review_entity_type}",
 *     "edit-form" = "/admin/structure/review_entity/{review_entity}/edit",
 *     "delete-form" = "/admin/structure/review_entity/{review_entity}/delete",
 *     "collection" = "/admin/structure/review_entity",
 *   },
 *   bundle_entity_type = "review_entity_type",
 *   field_ui_base_route = "entity.review_entity_type.edit_form"
 * )
 */
class ReviewEntity extends ContentEntityBase implements ReviewEntityInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Review entity entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['entity_reference'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Entity Reference'))
      ->setDescription(t('Entity Reference to content.'))
      ->setDisplayOptions('form',
        [
          'type' => 'string_textfield',
          'weight' => 1,
        ]
      )
      ->setRequired(TRUE);


    $fields['stars'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Stars Raiting ( 1-5 stars )'))
      ->setDescription(t('Set the review stars'))
      ->setDisplayOptions('form',
        [
          'type' => 'integer',
          'weight' => 2,
        ]

      )->addPropertyConstraints('value', [
          'Range' => [
            'min' => 1,
            'max' => 5,
          ],]
      )
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['overall_review'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Overall review'))
      ->setDescription(t('Overall review.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -2,
      ])
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Review entity is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
