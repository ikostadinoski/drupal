<?php

namespace Drupal\event_title_case;

class TitleCase
{
  public function filterTitle($title){
    $nonPrincipal = ['a','an','the','on','in','with','when','of','tho','and','by','to','when'];
    $titleArray = explode(' ', $title);
    foreach ($titleArray as $key => $word) {
      if($key === 0){
        $titleArray[$key] = ucfirst($word);
      }
      else if (!in_array($word, $nonPrincipal)){
        $titleArray[$key] = ucfirst($word);
      }
    }
    $titleArray =  implode(' ', $titleArray);
    return $titleArray;
  }
}
