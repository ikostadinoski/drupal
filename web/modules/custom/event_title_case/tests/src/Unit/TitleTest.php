<?php

namespace Drupal\Tests\event_title_case\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\event_title_case\TitleCase;

/**
 * Simple test to ensure that asserts pass.
 *
 * @group phpunit_example
 */
class TitleTest extends UnitTestCase {

  protected $unit;

  /**
   * Before a test method is run, setUp() is invoked.
   * Create new unit object.
   */
  public function setUp() {
    $this->unit = new TitleCase();
  }

  /**
   * @covers Drupal\event_title_case\Unit::filterTitle
   */
  public function testfilterTitle() {

    $title = $this->unit->filterTitle('in the shallows');
    $this->assertEquals('In the Shallows', $title);
  }

  /**
   * Once test method has finished running, whether it succeeded or failed, tearDown() will be invoked.
   * Unset the $unit object.
   */
  public function tearDown() {
    unset($this->unit);
  }

}
