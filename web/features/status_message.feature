  @api
Feature: Test account menu links

  @api @javascript
  Scenario: Make sure that logged in users see the account menu
    Given I am logged in as a user with the "admin" role
    When I go to "/admin/config/status/config"
    And I fill in wysiwyg on field "edit-message" with "Test123"
    When I press "edit-submit"
    Then I should see "The configuration options have been saved."

