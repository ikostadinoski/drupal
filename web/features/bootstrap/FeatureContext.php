<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Mink\Exception\ExpectationException;
use Drupal\DrupalExtension\Context\RawDrupalContext;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends RawDrupalContext {

  /**
   * The mink context.
   *
   * @var Drupal\DrupalExtension\Context\MinkContext
   */
  protected $minkContext;

  /**
   * Initializes context.
   *
   * Every scenario gets its own context instance.
   * You can also pass arbitrary arguments to the
   * context constructor through behat.yml.
   */
  public function __construct() {
  }

  /**
   * Fill in wysiwyg on field.
   *
   * @Then I fill in wysiwyg on field :locator with :value
   */
  public function iFillInWysiwygOnFieldWith($locator, $value) {
    $el = $this->getSession()->getPage()->findField($locator);
    if (empty($el)) {
      throw new ExpectationException('Could not find WYSIWYG with locator: ' . $locator, $this->getSession());
    }
    $fieldId = $el->getAttribute('id');
    if (empty($fieldId)) {
      throw new Exception('Could not find an id for field with locator: ' . $locator);
    }
    $this->getSession()
      ->executeScript("CKEDITOR.instances[\"$fieldId\"].setData(\"$value\");");
  }


    /**
     * @Given I am logged in as a user with the :arg1 role
     */
    public function iAmLoggedInAsAUserWithTheRole($arg1)
    {
        throw new PendingException();
    }

    /**
     * @When I go to :arg1
     */
    public function iGoTo($arg1)
    {
        throw new PendingException();
    }

    /**
     * @When I enter :arg1 for :arg2
     */
    public function iEnterFor($arg1, $arg2)
    {
        throw new PendingException();
    }

    /**
     * @When I press :arg1
     */
    public function iPress($arg1)
    {
        throw new PendingException();
    }

    /**
     * @Then I should see :arg1
     */
    public function iShouldSee($arg1)
    {
        throw new PendingException();
    }

    /**
     * @Given I am on :arg1
     */
    public function iAmOn($arg1)
    {
        throw new PendingException();
    }

    /**
     * @Then I should see form for message in :arg1 region
     */
    public function iShouldSeeFormForMessageInRegion($arg1)
    {
        throw new PendingException();
    }

    /**
     * @Then I should see dropdown for type in the :arg1 region
     */
    public function iShouldSeeDropdownForTypeInTheRegion($arg1)
    {
        throw new PendingException();
    }
}
